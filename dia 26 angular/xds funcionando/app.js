var phonecatApp = angular.module('phonecatApp', [
  'ngRoute',
  'phonecatServices',
  'phonecatAnimations',
  'phonecatControllers',
  'phonecatFilters',
  'chosser'
  ]);
phonecatApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/phones', {
        templateUrl: './phone-list.html',
        controller: 'PhoneListCtrl'
      }).
      when('/phones/:phoneId', {
        templateUrl: './phone-detail.html',
        controller: 'PhoneDetailCtrl'
      }).
      otherwise({
        redirectTo: '/phones'
      });
  }]);

var phonecatApp = angular.module('chosser',function(){
  var linker = function($scope, element , attr){
    element.chsosen();
  };
  return {
    restrict: 'A',
    link:linker
  }
});