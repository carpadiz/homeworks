/*
Find the capitals
Write a function capitals that takes a single string (word) as argument. 
The functions must return an ordered list containing the indexes of all 
capital letters in the string.

 capitals(65,90)

Example
capitals('CodEWaRs');			// [0,3,4,6]
*/
function capitals(str) {
  var count = [];
  for (var i = 0; i < str.length; i++) {
    var temp = str.charAt(i);
    if (temp >= "A" && temp <= "Z") {
      if(count===undefined){
      count[0] = i;
      }
      else{
        count[count.length] = i;
      }
    }
  }
  console.log(count);  
}

