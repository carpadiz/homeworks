/*
Unique
Implement an algorithm to determine if a string has all unique characters.

*/
function unique(string) {
  var repeated = {
  };
  string = string.toLowerCase();
  for (var f = 0; f < string.length; f++) {
    if (repeated[string[f]] === undefined) {
      repeated[string[f]] = 1;
    } 
    else {
      return false;
    }
  }
  return true;
}
/*unique('anit');*/
