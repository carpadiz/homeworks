/*
Fill with zeros
Write an algorithm such that if an element in an MxN matrix is 0, its entire 
row and column is set to 0
 */
function zeroFill(matrix) {
  var matrixTemp = JSON.parse(JSON.stringify(matrix));
  ;
  var height = matrix.length;
  var width = matrix[height - 1].length;
  for (var y = 0; y < height; y++) {
    for (var x = 0; x < width; x++) {
      if (matrix[y][x] === 0) {
        for (var yf = 0; yf < height; yf++) {
          matrixTemp[y].splice(yf, 1, 0);
          matrixTemp[yf].splice(x, 1, 0);
        }
      }
    }
  }
  console.log(matrixTemp.join('\n'));
}
zeroFill([[1,
2,
3,
4,
5],
[
  1,
  2,
  3,
  7,
  2
],
[
  1,
  2,
  3,
  4,
  5
],
[
  1,
  0,
  3,
  4,
  5
],
[
  1,
  2,
  3,
  4,
  5
]])
