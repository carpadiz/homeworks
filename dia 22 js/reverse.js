/*
Reverse
Write a function that receives a string and returns the string reversed as result.

*/
function reverse(string) {
  var line = '';
  for (var f = string.length; f >= 0; f--) {
    line += string.charAt(f);
  }
  console.log(line);
}
