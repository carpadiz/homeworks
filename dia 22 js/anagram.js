/*
Anagrams
Write a function that receives two strings and 
return true if the strings are 
anagrams, or false otherwise.

 */
function Anagram(word, word_analis) {
  var flag;
  if (word.length !== word_analis.length) {
    return false;
  }
  for (var p = 0; p < word.length; p++) {
    flag = false;
    for (var y = 0; y < word_analis.length; y++) {
      if (word[p] === word_analis[y]) {
        flag = true;
      }
    }
    if (!flag === true) {
      return false;
    }
  }
  return true;
}
