/*
remplace string
*/
function replaceSpace(str) {
  var line = '';
  for (var i = 0; i < str.length; i++) {
    if (str[i] !== ' ') {
      line += str[i];
    } 
    else {
      line += '%20'
    }
  }
  return line;
}
/*
is substring
*/

function isSubstring(x, y) {
  return y.indexOf(x) !== - 1;
}
/*
isSubstring("waterfalsy","wateri");

*/
/*
false
*/

function isSubRotedString(x, y) {
  var T = x.length;
  var t = y.length;
  if (T !== t) {
    return false;
  }
  for (var i = 0; i < T; i++) {
    var p = x.slice(0, 1);
    x = x.slice(1, t);
    x += p;
    if (isSubstring(x, y)) {
      return true;
    }
  }
  return false;
}
isSubRotedString('carlos', 'calos');
function bytwo(vector) {
  for (var i = 0; i < vector.length; i++) {
    vector[i] *= 2;
  }
  return vector;
}
/*
bytwo([1,2,3,4,5,6,7,8,9])
*/

function onlyEven(vector) {
  for (var i = 0; i < vector.length; i++) {
    if (vector[i] % 2 !== 0) {
      return false;
    }
  }
  return true;
}
/* 
onlyEven([2,4,6,8,10]);
*/

function someEven(vector) {
  for (var i = 0; i < vector.length; i++) {
    if (vector[i] % 2 === 0) {
      return true;
    }
  }
  return false;
}
/*
someEven([1,3,5,7])
*/

function givemeEven(vector) {
  var array = [
  ];
  for (var i = 0; i < vector.length; i++) {
    if (vector[i] % 2 === 0) {
      array.push(vector[i]);
    }
  }
  return array;
}
/*
someEven([1,2,3,4,5,6,7,8,9])
2,4,6,8
*/
