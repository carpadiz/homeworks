Actividad:

Crear el HTML y CSS necesario para igualar la tabla presentada en la imagen layout.jpg
Usar el header-background.gif y el logo.png para el header
El content area del table es tan ancho como el header-background.gif
El background del table footer es: footer-background-gradient.png
Las celdas tienen un alto de 48px
Los bordes de la tabla tienen un ancho de 4px y son de color #0c2a62
El fondo de las celdas del table header es de color #ffffff, el borde azul tiene un ancho de 2px y su hex es #0c2a62
El fondo de las celdas del table content, azul, es #dde4f3
El fondo de las otras celdas del table content es: #f0f0f0
El borde de cada celda, es gris #c7cddb y es de 1px de ancho

El font de los textos diferentes viene en la imagen layout.jgp como contenido de las celdas que representan

* La propiedad border-radius: les permite poner border curveados.
https://developer.mozilla.org/en-US/docs/Web/CSS/border-radius


Nombre de la carpeta: Actividad Fin de Semana 2
Terminan y lo suben a su repo.


Saludos!

L
