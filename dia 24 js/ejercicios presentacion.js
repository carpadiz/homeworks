/*
 * This is a JavaScript Scratchpad.
 *
 * Enter some JavaScript, then Right Click or choose from the Execute Menu:
 * 1. Run to evaluate the selected text (Ctrl+R),
 * 2. Inspect to bring up an Object Inspector on the result (Ctrl+I), or,
 * 3. Display to insert the result in a comment after the selection. (Ctrl+L)
*/
function onlyeven(array){
  return array.every(function(elem){
    if (elem%2 !== 0){
      return false;
    } 
    else{
      return true;
    }
  });
}
onlyeven([2,4,7]);
 
function haseven(val){
  return val%2===0;
}
[1,3,5].some(haseven);


var aray=[1,2,3,4,5,6,7,8,9,10];

function only_even (x){
  return x%2===0;
}
aray.filter(only_even);

function bytwo(e){
  return e*2;
}
 aray.map(bytwo);

function multiplyall(pv,cv){
  return cv*pv;
}

aray.reduce(multiplyall)

