/*
* Deep comparison
The == operator compares objects by identity. But sometimes,
you would prefer to compare the values of their actual properties.
Write a function, deepEqual, that takes two values and returns true 
only if they are the same value or are objects with the same properties
whose values are also equal when compared with a recursive call to deepEqual.

To find out whether to compare two things by identity 
(use the === operator for that) or by looking at their properties, 
you can use the typeof operator. If it produces "object" for both values,
you should do a deep comparison. But you have to take one silly 
exception into account: by a historical accident,typeof null 
also produces "object".
var obj = {here: {is: "an"}, object: 2}; 
console.log(deepEqual(obj, obj)); 
// → true console.log(deepEqual(obj, {here: 1, object: 2})); 
// → false console.log(deepEqual(obj, {here: {is: "an"}, object: 2})); 
// → true

 */
/*var d = {here: {is: "an"}, object: 2};
    var r = {here: {is: "an"}, object: 2};*/
function deepEqual(compare1, compare2) {
  if (typeof compare1 === 'object' && typeof compare2 === 'object') {
    if (compare1 === null || compare2 === null /*|| compare1.hasAttributes ||compare2.hasAttributes */) {
      return false;
    }
    x = compare1;
    y = compare2;
    for (index1 in compare1) {
      for (index2 in compare1) {
        return deepEqual(index1, index2) && deepEqual(compare1[index1], compare2[index2]);
      }
    }
  } 
  else if (compare1 === compare2) {
    return true;
  }
  return false;
}
/*
Flattening
Use the reduce method in combination with the concat method to
“flatten” an array of arrays into a single array that has
all the elements of the input arrays.
var arrays = [[1, 2, 3], [4, 5], [6]];
// Su función recibiendo el arrays y el resultado es el arreglo siguiente
// → [1, 2, 3, 4, 5, 6]
*/
var arrays = [[1, 2, 3, 4, 5, 6],[1, 2, 3], [4, 5], [6]];
function flattening(vectors){
  vectors=vectors||[];
  if(vectors!== undefined && vectors.length>0){
  var arrayR=[];
   arrayR= vectors.reduce(function(x,y){
    return x.concat(y);
  });
  }
  return arrayR || false;
}

flattening(arrays);