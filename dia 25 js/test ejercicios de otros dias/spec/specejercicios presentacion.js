var aray=[1,2,3,4,5,6,7,8,9,10];
var aray2=[1,3,5,7,9,11,13,15];
describe('this is all exercises of dia 24', function () {
  it('onlyeven(array) return bolean if array is complitly even ', function () {
    expect(onlyeven([2,4,6,8,10])).toBe(true);
    expect(onlyeven(aray2)).toBe(false);
    /*expect(onlyeven()).not.toBeDefined();*/
  });
  it('haseven(val) return bolean if array is encountered some even ', function () {
    expect(aray.some(haseven)).toBe(true);
    expect(aray2.some(haseven)).toBe(false);
    /*expect(aray.some()).not.toBeDefined();*/
  });
  it('filter(only_even) return array whit all even numbers ', function () {
    expect(aray.filter(only_even)).toMatch([ 2, 4, 6, 8, 10 ]);
    expect(aray2.filter(only_even)).toMatch([]);
    /*expect(aray.filter()).toBeDefined();*/
  });
  it('bytwo(e) using .map to return array multiplied by two', function () {
    expect(aray.map(bytwo)).toMatch([ 2, 4, 6, 8, 10, 12, 14, 16, 18, 20 ]);
    /*expect(aray.map()).toBeDefined();*/
  });
  it('multiplyall(pv,cv) using .map to return array multiplied by this array', function () {
    expect(aray.reduce(multiplyall)).toMatch(3628800);
    expect(aray2.reduce(multiplyall)).not.toBe(NaN);
    /*expect(aray.reduce()).toBeDefined();*/
  });
});
