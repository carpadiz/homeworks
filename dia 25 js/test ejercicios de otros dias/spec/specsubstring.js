describe('this is a conjunt of text for substring pactice', function () {
  it('remplace all spaces whit \'%20\'', function () {
    expect(replaceSpace('hola como estas')).not.toBe(null);
    expect(replaceSpace('hola como estas')).not.toBe(NaN);
    expect(replaceSpace(112123)).not.toBe(NaN);
  });
  
  it("isSubstring return true if string is encountered in the second string", function(){
    expect(isSubstring("wa","sdgsgswater")).toBe(true);
    expect(isSubstring("watefalsy","erfa")).toBe(false);
    /*expect(isSubstring()).not.toBeDefined();*/
  });
   it("isSubRotedString return bolean if string is sub sr¿tring of second string ", function(){
    expect(isSubRotedString('carlos', 'loscar')).toBe(true);
    expect(isSubRotedString('carlos', 'sandia')).toBe(false);
    /*expect(isSubRotedString()).not.toBeDefined();*/
  });
  it("onlyEven return bolean if array is complitly even ", function(){
    expect(onlyEven([2,4,6,8,10])).toBe(true);
    expect(onlyEven([1,2,3,4,5,6])).toBe(false);
    /*expect(onlyEven()).not.toBeDefined();*/
  });
   it("someEven(vector) return bolean if array is encountered some even ", function(){
    expect(someEven([2,4,6,8,10])).toBe(true);
    expect(someEven([1,3,5,7])).toBe(false);
    /*expect(someEven()).not.toBeDefined();*/
  });
     it("givemeEven(vector) return array whit all even numbers ", function(){
    expect(givemeEven([2,4,6,7,6,4,3,8,10])).toMatch([ 2, 4, 6, 6, 4, 8, 10 ]);
    /*expect(givemeEven()).toBeDefined();*/
  });
});
