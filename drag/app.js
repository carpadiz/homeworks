/**
*  Module
*
* Description
*/
angular.module('dragImg', ['ngDraggable']).
controller('MainCtrl', function($scope){
  $scope.draggableObjects = [{url:'./img/car (1).jpg'}, {url:'./img/car (2).jpg'}, {url:'./img/car (3).jpg'}, {url:'./img/car (4).jpg'}, {url:'./img/car (5).jpg'}, {url:'./img/car (6).jpg'}, {url:'./img/car (7).jpg'}, {url:'./img/car (8).jpg'}, {url:'./img/car (9).jpg'}]; 
  $scope.droppedObjects = [];
  $scope.onDropComplete1=function(data,evt){
    var index = $scope.droppedObjects.indexOf(data);
    if (index == -1){
      $scope.droppedObjects.push(data);
    }
  }  
  $scope.onDragSuccess1=function(data,evt){
    var index = $scope.droppedObjects.indexOf(data);
    if (index > -1) {
      $scope.droppedObjects.splice(index, 1);
    }
  }
  $scope.onDropComplete2=function(data,evt){
    var index = $scope.draggableObjects.indexOf(data);
    if (index == -1){
      $scope.draggableObjects.push(data);
    }
  }  
  $scope.onDragSuccess2=function(data,evt){
    var index = $scope.draggableObjects.indexOf(data);
    if (index > -1) {
      $scope.draggableObjects.splice(index, 1);
    }
  }
});