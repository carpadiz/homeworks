function drawcheckers(vertical, horizontal) {
  console.log('--------------------------------');
  for (var x = 1; x <= vertical; x++) {
    var line = '';
    for (var y = 1; y <= horizontal; y++) {
      if (y % 2 === 0 && x % 2 === 0) {
        line += '█';
      } 
      else if (y % 2 != 0 && x % 2 != 0) {
        line += '█';
      } 
      else {
        line += ' ';
      }
    }
    console.log(line);
  }
}
drawcheckers(10, 20);
